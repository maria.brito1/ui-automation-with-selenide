Feature: Tasks
  Scenario: Login
    Given i am a user on the ToDoApp Sign in page
    When i enter valid <username> and <password>
      | maria.brito@avenuecode.com.br | M4luTestes |
    Then i should be redirected to the main app page

  Scenario Outline: Create task successfully
    Given i am on the tasks page of the ToDoApp
    When i enter a new <task>
    And press enter
    Then the <task> should be appended on the list of created tasks

    Given i am on the tasks page of the ToDoApp
    When i enter a new <task>
    And press the "+" button
    Then the <task> should be appended on the list of created tasks

    Examples:
    | task |
    | "new test task" |

  Scenario Outline: Create task with error

    Given i am on the tasks page of the ToDoApp
    When i enter a new <task> with less than 3 characters
    And press the "+" button
    Then the <task> shouldn't be appended on the list of created tasks

    Given i am on the tasks page of the ToDoApp
    When i enter a new <task> with more than 250 characters
    And press the "+" button
    Then the <task> shouldn't be appended on the list of created tasks

    Examples:
    | task |
    | "#1" |
    | "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Rum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium..." |

  Scenario: See tasks on ToDoApp
    Given i am a user on the ToDoApp homepage
    When i click on the "My Tasks" link
    Then i should be redirected to a page with all the created tasks so far
    And the top part of the page should show the message "Hey <user>, this is your todo list for today:"
      |"Maria Luisa"|






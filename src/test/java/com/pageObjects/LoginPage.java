package com.pageObjects;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    public SelenideElement username = $("[id=\"user_email\"]");
    public SelenideElement password = $("[id=\"user_password\"]");
    public SelenideElement rememberMe = $("[id=\"user_remember_me\"]");
    public SelenideElement signInButton = $("[name=\"commit\"]");


}

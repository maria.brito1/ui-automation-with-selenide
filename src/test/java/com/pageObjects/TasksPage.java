package com.pageObjects;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TasksPage {

    public SelenideElement taskInput = $("[id=new_task]");
    public SelenideElement createTaskButton = $("[class=\"input-group-addon glyphicon glyphicon-plus\"]");
    public SelenideElement myTasksLink = $("[id=\"my_task\"]");
    public SelenideElement greeting = $x("/html/body/div[1]/h1");
    public SelenideElement removeButton = $("[class=\"btn btn-xs btn-danger\"]");
    public ElementsCollection createdTasks = $$("[class=\"table\"]");
//    public ElementsCollection createdTasks = $$x("");

}

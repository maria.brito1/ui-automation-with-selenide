package com.steps;

import com.codeborne.selenide.ElementsCollection;
import com.pageObjects.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;

import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

public class Steps {
    HomePage homePage = new HomePage();
    TasksPage tasksPage = new TasksPage();
    LoginPage loginPage = new LoginPage();

    @Given("i am a user on the ToDoApp homepage")
    public void getHomePage(){
        open("https://qa-test.avenuecode.com/");
    }

    @Given("i am a user on the ToDoApp Sign in page")
    public void getSignInPage(){
        open("https://qa-test.avenuecode.com/users/sign_in");
    }

    @Given("i am on the tasks page of the ToDoApp")
    public void getTasksPage() {
        open("https://qa-test.avenuecode.com/tasks");
    }

    @When("i enter a new task")
    public void createNewTask() {
        tasksPage.taskInput.sendKeys();
    }

    @When("i enter a new {string}")
    public void createNewTask(String task) {
        tasksPage.taskInput.sendKeys(task);
    }

    @When("i enter a new {string} with less than 3 characters")
    public void create2CharacterTask(String task){
        tasksPage.taskInput.sendKeys(task);
    }

    @When("i enter a new {string} with more than 250 characters")
    public void create251CharactersTask(String task){
        tasksPage.taskInput.sendKeys(task);
    }

    @And("press enter")
    public void createTaskPressingEnter() {
        tasksPage.taskInput.pressEnter();
    }

    @And("press the \"+\" button")
    public void createTaskClickingOnButton() {
        tasksPage.createTaskButton.click();
    }

    @When("i enter valid <username> and <password>")
    public void login(DataTable login){
       List<String> credentials =  login.asList();
         loginPage.username.sendKeys(credentials.get(0));
         loginPage.password.sendKeys(credentials.get(1));
         loginPage.rememberMe.click();
         loginPage.signInButton.click();
    }

    @When("i click on the \"My Tasks\" link")
    public void clickOnMyTasksLink() {
        tasksPage.myTasksLink.click();
    }

    @Then("the {string} should be appended on the list of created tasks")
    public void checkingTaskCreation(String task) {
        Collection<ElementsCollection> tableContent = Collections.singleton(tasksPage.createdTasks);

        assertThat(tableContent, containsString(task));
    }

    @Then("the {string} shouldn't be appended on the list of created tasks")
    public void checkingTask(String task) {


    }

    @Then("i should be redirected to the main app page")
    public void checkMainPage() {
        homePage.welcomeText.shouldHave(text("QA Assessment"));
    }

    @Then("i should be redirected to a page with all the created tasks so far")
    public void checkTasksPage() {
    }

    @And("the top part of the page should show the message \"Hey <user>, this is your todo list for today:\"")
    public void myTasksLinkValidation(DataTable userName) {
        tasksPage.greeting.shouldHave(text("Hey " + userName.asList().get(0) + ", this is your todo list for today:"));
    }

}
